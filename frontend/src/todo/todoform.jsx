import React from 'react'
import Grid from './../templates/grid'
import IconButton from './../templates/iconbutton'


export default props => {
    const keyHandler = (e) => {
        if (e.key === 'Enter') {
            e.shiftKey ? props.handleSearch() : props.handleAdd()
        } else if (e.key === 'Escape') {
            props.handleClear()
        }
    }

    return (
        <div role='form' className='todoForm'>
            <Grid cols='12 9 10'>
                <input id="description" type="text" className="form-control" 
                    onChange={props.handleChange}
                    onKeyUp={keyHandler}
                    placeholder="Adicione uma tarefa" value={props.description}/>
            </Grid>
            <Grid cols='12 3 2'>
                <IconButton style='primary' icon='plus' onClick={props.handleAdd} title='Adicionar. Atalho: Enter' />
                <IconButton style='info' icon='search' onClick={props.handleSearch} title='Pesquisar. Atalho: Shift+Enter' />
                <IconButton style='default' icon='close' onClick={props.handleClear} title='Limpar. Atalho: Esc' />
            </Grid>
        </div>
    )
}